<?php


namespace App\Repository;


use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function filteredBy()
    {
        $query = $this->createQueryBuilder('c')
            ->select('c')
            ->leftJoin('App:Product', 'p', Join::WITH, 'c.id = p.category');

        return $query->getQuery()->getScalarResult();
    }

}