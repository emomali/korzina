<?php


namespace App\Form;


use App\Entity\Orders;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fio', TextType::class, [
            'required' => true,
            'label'    => 'ФИО'
        ])
            ->add('phone', TextType::class, [
                'required' => true,
                'label'    => 'Телефон'
            ])
            ->add('street', TextType::class, [
                'required' => true,
                'label'    => 'Улица'
            ])
            ->add('house', TextType::class, [
                'required' => true,
                'label'    => 'Дом'
            ])
            ->add('apartment', TextType::class, [
                'required' => true,
                'label'    => 'квартира'
            ])
            ->add('door', NumberType::class, [
                'required' => true,
                'label'    => 'Подъезд'
            ])
            ->add('comment', TextType::class, [
                'label'    => 'Коментарий',
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Orders::class,
        ]);
    }

}