<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Product
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="product")
 */
class Product implements \JsonSerializable
{

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer", unique=true, options={"comment":"Идентификатор товара"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", options={"comment":"Название товара"})
     */
    private $name;

    /**
     * @var Category
     *
     * @ManyToOne(targetEntity="App\Entity\Category", inversedBy="products", fetch="EAGER")
     * @JoinColumn(name="id_category", referencedColumnName="id")
     */
    private $category;

    /**
     * One product has many features. This is the inverse side.
     * @OneToMany(targetEntity="Feature", mappedBy="product", fetch="EAGER", cascade={"persist"})
     */
    private $features;

    /**
     * @ORM\Column(name="image", type="string", nullable=true, length=255, options={"comment":"Ссылка на прикрепленный файл"})
     * @Assert\File(
     *     mimeTypes = {"image/*"},
     *     mimeTypesMessage="Прикреплять можно только изображения"
     * )
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=true, length=255, options={"comment":"Описание товара"})
     */
    private $description;

    public function __construct() {
        $this->features = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|Feature[]
     */
    public function getFeatures(): Collection
    {
        return $this->features;
    }

    public function addFeature(Feature $feature): self
    {
        if (!$this->features->contains($feature)) {
            $this->features[] = $feature;
            $feature->setProduct($this);
        }

        return $this;
    }

    public function removeFeature(Feature $feature): self
    {
        if ($this->features->contains($feature)) {
            $this->features->removeElement($feature);
            // set the owning side to null (unless already changed)
            if ($feature->getProduct() === $this) {
                $feature->setProduct(null);
            }
        }

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'id'=>$this->getId(),
            'name'=>$this->getName(),
            'category'=>$this->category,
            'features'=>$this->getFeatures(),
            'image'=> $this->image,
            'description'=>$this->description
        ];
    }
}