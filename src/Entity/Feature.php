<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Validator\Constraints\Collection;

/**
 * Class Feature
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="feature")
 */
class Feature implements \JsonSerializable
{

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer", unique=true, options={"comment":"Идентификатор особенностей товара"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="size", type="string", nullable=true, options={"comment":"Размер"})
     */
    private $size;

    /**
     * @var int
     *
     * @ORM\Column(name="old_price", type="integer", nullable=true, options={"comment":"Старая цена"})
     */
    private $oldPrice;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer", options={"comment":"Новаря цена. Если поле пустое, то дейстует старая цена"})
     */
    private $price;

    /**
     * Many features have one product. This is the owning side.
     * @ManyToOne(targetEntity="Product", inversedBy="features", fetch="EAGER")
     * @JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(?string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getOldPrice(): ?int
    {
        return $this->oldPrice;
    }

    public function setOldPrice(int $oldPrice): self
    {
        $this->oldPrice = $oldPrice;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'id'=>$this->getId(),
            'size'=>$this->getSize(),
            'oldPrise'=>$this->getOldPrice(),
            'price'=>$this->getPrice(),
            'product'=>$this->getProduct()
        ];
    }
}