<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Orders
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="orders")
 */
class Orders
{

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer", unique=true, options={"comment":"Идентификатор"})
     */
    private $id;

    /**
     * Many Users have Many Groups.
     * @ManyToMany(targetEntity="Feature", inversedBy="orders")
     * @JoinTable(name="feature_orders",
     *      joinColumns={@JoinColumn(name="order_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="feature_id", referencedColumnName="id")}
     *      )
     */
    private $feature;

    /**
     * @var string
     *
     * @ORM\Column(name="fio", type="string", length=50, options={"comment":"ФИО заказчика"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $fio;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=12, options={"comment":"Телефон для контакта"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     * @Assert\Length(
     *     min=10,
     *     max=17,
     *     minMessage="Введен не корректный номер",
     *     maxMessage="Веден не корректный номер"
     * )
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="street",
     *     type="string", length=255, options={"comment":"Улица доставки"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $street;

    /**
     * @var string
     * @ORM\Column(name="house", type="string", length=50, options={"comment":"Дом"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $house;

    /**
     * @var string
     *
     * @ORM\Column(name="apartment", type="string", length=10, options={"comment":"Номер квартиры. Строка потому, что могут быть и с символами"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $apartment;

    /**
     * @var int
     *
     * @ORM\Column(name="door", type="integer", options={"comment":"Подъезд"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     * @Assert\Positive()
     */
    private $door;

    /**
     * @var string
     * @ORM\Column(name="comment", type="string", length=255, nullable=true, options={"comment":"Коментарий заказчика"})
     */
    private $comment;

    public function __construct()
    {
        $this->feature = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFio(): ?string
    {
        return $this->fio;
    }

    public function setFio(string $fio): self
    {
        $this->fio = $fio;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getHouse(): ?string
    {
        return $this->house;
    }

    public function setHouse(string $house): self
    {
        $this->house = $house;

        return $this;
    }

    public function getApartment(): ?string
    {
        return $this->apartment;
    }

    public function setApartment(string $apartment): self
    {
        $this->apartment = $apartment;

        return $this;
    }

    public function getDoor(): ?int
    {
        return $this->door;
    }

    public function setDoor(int $door): self
    {
        $this->door = $door;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Collection|Feature[]
     */
    public function getFeature(): Collection
    {
        return $this->feature;
    }

    /**
     * @param Feature $feature
     * @return $this|array
     */
    public function addFeature(Feature $feature)
    {
        $this->feature[] = $feature;
        return $this;
    }

    public function removeFeature(Feature $feature): self
    {
        if ($this->feature->contains($feature)) {
            $this->feature->removeElement($feature);
        }

        return $this;
    }

}