<?php


namespace App\Controller;

use App\Entity\Category;
use App\Entity\Feature;
use App\Entity\Orders;
use App\Entity\Product;
use App\Form\OrderType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Product::class)->findAll();
        $feature = $entityManager->getRepository(Feature::class)->findAll();
        $caregory = $entityManager->getRepository(Category::class)->findAll();
        $products = $this->get('session')->get('products');

        $sum = 0;
        if ($products){
            foreach ($products as $item) {
                $sum += $item['summ'];
            }
        }

        return $this->render('base.html.twig', [
            'products' => $product,
            'features' => $feature,
            'categorys' => $caregory,
            'basket' => $this->renderView('cart.html.twig', ['products' => $products, 'sum' => $sum])
        ]);
    }

    /**
     * @Route("/order", name="order")
     *
     * @throws \Exception
     */
    public function orderAction(Request $request)
    {
        $features = $this->get('session')->get('products');
        $order = new Orders();
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);
        if ($request->isXmlHttpRequest()) {
            if ($form->isSubmitted() && $form->isValid()) {

                foreach ($features as $feature) {
                    $f = $this->getDoctrine()->getRepository(Feature::class)->find($feature->getId());
                    $order->addFeature($f);
                }
                $em = $this->getDoctrine()->getManager();
                $em->persist($order);
                $em->flush();
                $this->get('session')->clear();

                return new JsonResponse([
                    'status' => 'success'
                ], 200);
            } else {
                return new JsonResponse(['status' => 'error']);
            }
        }
        $products = $this->get('session')->get('products');
        $sum = 0;
        if ($products){
            foreach ($products as $item) {
                $sum += $item['summ'];
            }
        }

        return $this->render('order.html.twig', [
            'features' => $features,
            'products' => $products,
            'sum' => $sum,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="deleteOrder", requirements={"id"="\d+"})
     *
     * @return Response
     */
    public function deleteAction($id)
    {
        $features = $this->get('session')->get('products');
        $products = [];
        foreach ($features as $feature) {
            if ($feature['feature']->getId() != $id) {
                array_push($products, $feature);
            }
        }
        $this->get('session')->clear();
        $this->get('session')->set('products', $products);

        return $this->redirectToRoute('order');
    }

    /**
     * @Route("/basket", name="basket", methods={"GET|POST"})
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function basket(Request $request)
    {
        $id_feature = $request->get('feature_id');

        $products = $this->get('session')->get('products');

        if ($id_feature) {
            $feature = $this->getDoctrine()->getRepository(Feature::class)->find($id_feature);
            if (!$feature) {
                return new JsonResponse('error', 301);
            }
            if (!$products)
                $products = [];
            $flag = false;
            /** @var Feature $item */
            foreach ($products as $item) {
                if ($item['feature']->getId() == $feature->getId()) {
                    $flag = true;
                }
            }

            if (!$flag) {
                array_push($products, ['feature' => $feature, 'count' => 1, 'summ' => $feature->getPrice()]);

            } else {
                $prod = [];
                foreach ($products as $item) {
                    if ($item['feature']->getId() == $feature->getId()) {
                        $item['count']++;
                        $item['summ'] = $item['count'] * $feature->getPrice();
                    }

                    array_push($prod, $item);
                }
                $products = $prod;
            }

            $sum = 0;
            foreach ($products as $item) {
                $sum += $item['summ'];
            }

            $this->get('session')->remove('products');
            $this->get('session')->set('products', $products);
        }

        return new Response($this->renderView('cart.html.twig', ['products' => $products, 'sum' => $sum]));
    }

    /**
     * @Route("/clear", name="clear", methods={"GET|POST"})
     */
    public function clear()
    {
        $this->get('session')->clear();
        return $this->redirectToRoute('homepage');
    }

}